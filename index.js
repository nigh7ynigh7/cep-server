const express = require('express');
const errResolver = require('./errors/resolver')

const PORT = process.env.PORT || 3000;

const app = express();

app.use(express.urlencoded({extended : true}));
app.use(express.json());

app.use('/cep', require('./routes/cep'));

app.use(function (err, req, res, next) {
  const {status, message} = errResolver(err);
  res.status(status).json({
    status, message
  })
});

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});