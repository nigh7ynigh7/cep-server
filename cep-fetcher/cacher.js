/** @type {{ [key : string ] : CepLocal} } */
const _cache = {};

// levels of caching
// 1- memory
// 2- redis
// 3- database


/**
 * function to locally store the CEP
 * @param {string} cep
 * @param {CepLocal} data
 * @returns {CepLocal}
 */
const setCache = async (cep, data) => {
  _cache[cep] = data;
  return data;
}

/**
 * @param {string} cep 
 * @returns {CepLocal | undefined}
 */
const getCache = async (cep) => {
  return _cache[cep];
}

module.exports = {
  setCache,
  getCache,
}