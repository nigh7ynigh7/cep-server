

const { CepNotFound, CepNotValid, RemoteServiceFailure } = require('../errors/catalog')
const fetch = require('node-fetch');
const { getCache, setCache } = require('./cacher');

const URL_A = `https://buscacepinter.correios.com.br/app/endereco/carrega-cep-endereco.php?pagina=/app/endereco/index.php&cepaux=&mensagem_alerta=&endereco={{cep}}&tipoCEP=ALL`;

/** @type {FetchCepAttempts[]} */
const attempts = [
  
  // attempts to fetch it locally
  (cep) => Promise.resolve(getCache(cep)),

  // attempts to fetch it via redis
  // TODO: redis impl

  // attempts to fetch it directly from Correios
  async (cep) => {
    return fetch(URL_A.replace('{{cep}}', cep), {
      method : 'get',
      headers : {
        'Referer' : 'https://buscacepinter.correios.com.br/app/endereco/index.php',
        'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0',
      }
    }).then((data) => {
      if(!data.ok) throw new RemoteServiceFailure('Could not fetch data');
      return data.json();
    }).then(json => {
      return ((json || {}).dados || []).map(item => ({
        state : item.uf,
        city : item.localidade,
        street : item.logradouroDNEC,
        neighborhood : item.bairro,
        neighbourhood : item.bairro,
        postalCode : item.cep,
        additional : [
          item.logradouro,
          item.logradouroTextoAdicional,
        ].filter(item => !!item)
      }))[0]
    }).then(json => {
      console.log(json);
      if(json) {
        setCache(cep, json);
      }
      return json;
    });
  }
]

const filterValue = (value) => {
  if(typeof value != 'string'
      || value.search(/\D/) > 0
      || value.length != 8) {
    return undefined;
  }
  return value;
}

module.exports = async (value) => {
  value = filterValue(value);
  if (!value) {
    throw new CepNotValid('Bad request');
  }
  found = await getCache(value);
  if(found){
    return found;
  }
  for (const attempt of attempts) {
    let val = await attempt(value);
    if(val) return val;
  }
  throw new CepNotFound('Could not find');
}