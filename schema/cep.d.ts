
interface CepLocal {
  state : string;
  city : string;
  street : string;
  /** same data as neighbourhood, uses us spelling */
  neighborhood : string;
  /** same data as neighborhood, uses british spelling */
  neighbourhood : string;
  postalCode : string;
  additional : string[];
}

type CepCollection = CepLocal[];

interface FetchCepAttempts {
  (cep ?: string) : Promise<CepLocal | undefined>
}
