const { CepNotFound, CepNotValid, RemoteServiceFailure } = require('./catalog')

/**
 * @param {Error} error
 * @returns {{
 *   status : number,
 *   message : string,
 * }}
 */
module.exports = (error) => {
  if(error instanceof CepNotFound)
    return { message : 'CEP_NOT_FOUND', status : 404 };
  else if (error instanceof CepNotValid)
    return { message : 'INPUT_CEP_NOT_VALID', status : 400 };
  else if (error instanceof RemoteServiceFailure)
    return { message : 'REMOTE_SERVICE_DOWN', status : 500 };
  return { message : 'ERROR', status : 500 };
}