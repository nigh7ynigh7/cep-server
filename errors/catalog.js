class CepNotFound extends Error {
  constructor(...data) {
    super(...data)
  }
}

class CepNotValid extends Error {
  constructor(...data) {
    super(...data)
  }
}

class RemoteServiceFailure extends Error {
  constructor(...data) {
    super(...data)
  }
}

module.exports = {
  CepNotFound,
  CepNotValid,
  RemoteServiceFailure
}