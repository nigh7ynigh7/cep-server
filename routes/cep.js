const cepFetcher = require('../cep-fetcher/fetcher');
const { Router } = require('express');

const route = Router();

route.get('/:cep', (req, res, next)=>{
  return cepFetcher(req.params.cep)
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      next(err);
    });
});


module.exports = route;